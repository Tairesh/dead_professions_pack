Dead Professions Pack
=====================

Mod for [Cataclysm: DDA](https://github.com/CleverRaven/Cataclysm-DDA/) with a bundle of dead (russian: *Дед*) almost unplayable professions for experienced and autistic players.
